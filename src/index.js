import React from 'react'
import ReactDOM from 'react-dom'
import data from './data'
console.log(data)
const First = ()=>{
  return(
    <div>
      <h1>Title</h1>
      <table>
        <thead>
          <tr>
            <th>Наименование</th>
            <th>Покупка</th>
            <th>Продажа</th>
          </tr>
        </thead>
        <tbody>
          {
            data.map((el,index)=>{
              return (
                <tr key={index + 1}>
                  <td>{el.ccy}</td>
                  <td>{parseFloat(el.buy).toFixed(2)}</td>
                  <td>{parseFloat(el.sale).toFixed(2)}</td>
                </tr>
              )
            })
          }
        </tbody>
      </table>
    </div>
  )
}
ReactDOM.render(<First/>, document.getElementById('root'))
